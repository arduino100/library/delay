#include "Arduino.h"
class Delay {
private:
  unsigned long lastTime;
  int delay;
  
public:
  Delay(int delay) {
    this->delay = delay;
    lastTime = -delay;
  }

  boolean isSuspended() {
    return millis() - lastTime < delay;
  }

  void suspend() {
    lastTime = millis();
  }
};